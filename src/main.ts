import * as fs from "fs"

const samples = {
  "1_simple_case": 7,
  "2_1000_groups_of_few_people": 3935,
  "3_the_same_groups_go_on_the_ride_several_times_during_the_day": 15,
  "4_all_the_people_get_on_the_roller_coaster_at_least_once": 15000,
  "5_high_earnings_during_the_day": 4999975000,
  "6_works_with_a_large_dataset": 89744892565569,
  "7_hard": 8974489271113753,
  "8_harder": 89744892714152289,
}

type SampleKey = keyof typeof samples

type SampleData = {
  nbRides: number
  nbGroups: number
  nbSeats: number
  groups: number[]
}

const getDataFromSampleFile = (sampleName: SampleKey): SampleData => {
  const filePath = `./samples/${sampleName}.txt`
  if (fs.existsSync(filePath)) {
    const buffer = fs.readFileSync(filePath)
    const fileArray = buffer.toString().split("\n")
    const [firstLine, ...rest] = fileArray
    if (!firstLine) {
      throw new Error("beurk")
    }
    const [seats, rides, nbGroups] = firstLine.split(" ").map(Number)

    if (!seats || !rides || !nbGroups) {
      throw new Error("beurk")
    }

    return {
      nbRides: rides,
      nbGroups: nbGroups,
      nbSeats: seats,
      groups: rest.filter((_) => _ !== " ").map(Number),
    }
  }

  throw new Error("beurk")
}

const groupsFn = (arr: number[]) => ({
  hasReachedEndOfQueue: (currentIndex: number, startingIndex: number) =>
    currentIndex >= arr.length || startingIndex - currentIndex === 0,
  nextIndex: (currentIndex: number) =>
    currentIndex + 1 === arr.length ? currentIndex + 1 - arr.length : currentIndex + 1,
})

const cacheFn = (cache: Map<number, { nextIndex: number; earnings: number }>) => ({
  findLoopCycleIndexes: (startingIndex: number) => {
    let nextIndex = cache.get(startingIndex)!.nextIndex
    const cycle: number[] = [startingIndex]
    while (nextIndex != startingIndex) {
      cycle.push(nextIndex)
      nextIndex = cache.get(nextIndex)!.nextIndex
    }
    return cycle
  },
  getLoopEarnings: (loop: number[]) => loop.reduce((acc, v) => (acc += cache.get(v)!.earnings), 0),
})

const getEarningsOutsideLoop = (
  cache: Map<number, { nextIndex: number; earnings: number }>,
  nbOfRides: number,
  startingIndex: number,
) => {
  let tmp = nbOfRides
  let index = startingIndex
  let earnings = 0
  while (tmp > 0) {
    const cachedValue = cache.get(index)!
    earnings += cachedValue.earnings
    index = cachedValue.nextIndex
    tmp -= 1
  }
  return earnings
}

const getEarnings = (sampleData: SampleData): number => {
  const cache = new Map<number, { nextIndex: number; earnings: number }>()
  let totalEarnings = 0
  let remainingNbOfRides = sampleData.nbRides
  let currentIndex = 0
  let remainingSeats = sampleData.nbSeats
  let currentEarnings = 0
  let startingIndex = 0
  const cacheHelpers = cacheFn(cache)
  const groupsHelpers = groupsFn(sampleData.groups)
  while (remainingNbOfRides != 0) {
    if (startingIndex === currentIndex && cache.has(currentIndex)) {
      const cachedValue = cache.get(currentIndex)!
      const loop = cacheHelpers.findLoopCycleIndexes(cachedValue.nextIndex)
      const loopEarnings = cacheHelpers.getLoopEarnings(loop)
      const earlyEarnings = totalEarnings - loopEarnings
      const totalNbOfLoops = Math.floor((sampleData.nbRides - (sampleData.nbRides - remainingNbOfRides)) / loop.length)
      const totalLoopEarnings = loopEarnings * totalNbOfLoops
      const remainingNumberOfRidesOutsideOfLoop =
        sampleData.nbRides - totalNbOfLoops * loop.length - (sampleData.nbRides - remainingNbOfRides - loop.length) - 1
      const earningsOutsideOfLoop = getEarningsOutsideLoop(
        cache,
        remainingNumberOfRidesOutsideOfLoop,
        cachedValue.nextIndex,
      )
      return earningsOutsideOfLoop + totalLoopEarnings + earlyEarnings + cachedValue.earnings
    }

    const nbPeople = sampleData.groups[currentIndex]!
    if (nbPeople > remainingSeats) {
      cache.set(startingIndex, { nextIndex: currentIndex, earnings: currentEarnings })
      startingIndex = currentIndex
      totalEarnings += currentEarnings
      currentEarnings = 0
      remainingNbOfRides -= 1
      remainingSeats = sampleData.nbSeats
    } else if (nbPeople === remainingSeats) {
      currentEarnings += nbPeople
      currentIndex = groupsHelpers.nextIndex(currentIndex)

      cache.set(startingIndex, { nextIndex: currentIndex, earnings: currentEarnings })
      startingIndex = currentIndex
      totalEarnings += currentEarnings
      currentEarnings = 0
      remainingNbOfRides -= 1
      remainingSeats = sampleData.nbSeats
    } else {
      remainingSeats -= nbPeople
      currentEarnings += nbPeople
      currentIndex = groupsHelpers.nextIndex(currentIndex)
      if (groupsHelpers.hasReachedEndOfQueue(currentIndex, startingIndex)) {
        remainingNbOfRides -= 1
        remainingSeats = sampleData.nbSeats
        cache.set(startingIndex, { nextIndex: currentIndex, earnings: currentEarnings })
        totalEarnings += currentEarnings
        startingIndex = currentIndex
        currentEarnings = 0
      }
    }
  }

  return totalEarnings
}

const evaluateAllRides = () =>
  Object.keys(samples).forEach((k) => {
    const data = getDataFromSampleFile(k as SampleKey)
    const earnings = getEarnings(data)
    console.log("Running  ", k)
    console.log("Expected ", samples[k as SampleKey])
    console.log("Got      ", earnings)
    console.log("Success ?", earnings === samples[k as SampleKey])
  })

export const evaluateRide = (sampleName: SampleKey) => {
  const data = getDataFromSampleFile(sampleName)
  return getEarnings(data)
}

if (process.env["NODE_ENV"] !== "test") {
  evaluateAllRides()
}
