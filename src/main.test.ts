import { evaluateRide } from "./main"

describe("evaluateRide", () => {
  it("should return 7 for test 1", () => {
    const earnings = evaluateRide("1_simple_case")
    expect(earnings).toBe(7)
  })

  it("should return 3935 for test 2", () => {
    const earnings = evaluateRide("2_1000_groups_of_few_people")
    expect(earnings).toBe(3935)
  })

  it("should return 15 for test 3", () => {
    const earnings = evaluateRide("3_the_same_groups_go_on_the_ride_several_times_during_the_day")
    expect(earnings).toBe(15)
  })

  it("should return 15000 for test 4", () => {
    const earnings = evaluateRide("4_all_the_people_get_on_the_roller_coaster_at_least_once")
    expect(earnings).toBe(15000)
  })

  it("should return 4999975000 for test 5", () => {
    const earnings = evaluateRide("5_high_earnings_during_the_day")
    expect(earnings).toBe(4999975000)
  })

  it("should return 89744892565569 for test 6", () => {
    const earnings = evaluateRide("6_works_with_a_large_dataset")
    expect(earnings).toBe(89744892565569)
  })

  it("should return 8974489271113753 for test 7", () => {
    const earnings = evaluateRide("7_hard")
    expect(earnings).toBe(8974489271113753)
  })

  it("should return 89744892714152289 for test 8", () => {
    const earnings = evaluateRide("8_harder")
    expect(earnings).toBe(89744892714152289)
  })
})
